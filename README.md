# irclogger for nixos

This is a WIP configuration for [irclogger](https://github.com/whitequark/irclogger).

It may break, but it shouldn't.

## TLDR usage

```nix
{
  #...
  services.nginx = {
    enable = true;
    commonHttpConfig = ''
      upstream irclogger {
        server unix:/var/lib/irclogger/tmp/irclogger-viewer.sock;
      }
    '';
    virtualHosts = {
      "_default" = {
        default = true;
        locations."/" = {
          root = "${pkgs.irclogger}/share/irclogger/public";
          extraConfig = ''
            if (!-f $request_filename) {
              proxy_pass http://irclogger;
            }   
          '';
        };
      };
    };
  };
  #...

  services.postgresql.enable = true;
  services.redis.enable = true;
  services.irclogger.enable = true;

  environment.etc."irclogger/config.yml" = {
    text = ''
      ---
      database: postgres:///irclogger
      redis: redis://localhost:6379
      
      server: irc.example.com
      realname: nixos-users logging bot
      nickname: irclogger
      username: irclogger
      channels:
        - "#irclogger-testing"
      domain: irclogger # UNIX DOMAIN SOCKET NAME FOR NGINX (it seems) Otherwise, forward proper headers?
    '';
  };

}
```

Then:

```
sudo systemctl restart irclogger-stateful.service
nix-shell -p irclogger --run "irclogger-init-database"
sudo systemctl restart irclogger-stateful.service irclogger-viewer.service irclogger-logger.service
```
