# Implements the "module" for nixos.
# Import in your `configuration.nix`
{ config, pkgs, lib, ... }:

with lib;
let
  cfg = config.services.irclogger;
  defaultUser = "irclogger";
  homeDir = "/var/lib/irclogger";
in
{

  options = {
    services.irclogger = {
      enable = mkOption {
        default = false;
        description = "Whether to enable irclogger service.";
      };

      usePostgreSQL = mkOption {
        default = true;
        description = "Whether PostgreSQL is used to store the logs.";
      };

      config = mkOption {
        type = types.string;
        default = "/etc/irclogger/config.yml";
        description = ''
          Configuration file for the logger.
          Defaults to a path in /etc that does not exist.
        '';
      };

      user = mkOption {
        default = "irclogger";
        example = "john";
        type = types.string;
        description = ''
          The name of an existing user account to use to own the irc logger processes.
          If not specified, a default user will be created to own the processes.
        '';
      };

      group = mkOption {
        default = "";
        example = "users";
        type = types.string;
        description = ''
          Group to own the irclogger process.
        '';
      };

      dbName = mkOption {
        default = "irclogger";
        description = "Name of the database that holds the data.";
      };

    };
  };

  # ----------------------------------------------------------------------

  config = mkIf cfg.enable {
    # FIXME : Allow logging to another instance of PostgreSQL.
    #assertions = singleton {
    #  assertion = !(cfg.usePostgreSQL -> config.services.postgresql.enable);
    #  message = "irclogger is configured to use PostgreSQL but services.postgresql.enable is not enabled.";
    #};

    systemd.services.irclogger-stateful = {
      # Ensures database and database user exists.
      description = "Whitequark's IRC Logger (stateful setup)";
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = config.services.postgresql.superUser;
      };
      after = [ "networking.target" ] ++
        optional cfg.usePostgreSQL "postgresql.service";

      # FIXME : Can't get the database's objects to be owned by `cfg.user` without a pile of hacks.
      # For now, this will create ONLY the database, user will need to seed database semi-manually.
      # `nix-shell -p irclogger --run "irclogger-init-database"`
      preStart = ''
        if ! ${pkgs.postgresql}/bin/psql -l | grep -q ' ${cfg.dbName} ' ; then
          ${pkgs.postgresql}/bin/createuser --no-superuser --no-createdb --no-createrole "${cfg.user}" || true
          ${pkgs.postgresql}/bin/createdb "${cfg.dbName}" --owner="${cfg.user}"
          echo 'CREATE EXTENSION IF NOT EXISTS btree_gin;' \
            | ${pkgs.postgresql}/bin/psql "${cfg.dbName}"
          #(
          #echo "ALTER DEFAULT PRIVILEGES IN SCHEMA public"
          #echo "GRANT INSERT, UPDATE, DELETE ON TABLES TO ${cfg.user};"
          #) | ${pkgs.postgresql}/bin/psql "${cfg.dbName}"
          #(
          #  cat ${pkgs.irclogger}/share/irclogger/config/sql/postgresql-schema.sql | grep -v '^#'
          #) | ${pkgs.postgresql}/bin/psql "${cfg.dbName}"
          #(
          #  echo "ALTER SCHEMA public OWNER TO ${cfg.user};"
          #  echo "ALTER DATABASE ${cfg.dbName} OWNER TO ${cfg.user};"
          #  echo "GRANT ALL PRIVILEGES ON DATABASE ${cfg.dbName} to ${cfg.user};"
          #  echo "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO ${cfg.user};"
          #) | ${pkgs.postgresql}/bin/psql "${cfg.dbName}"
        fi
      '';
      script = "true";
    };

    systemd.services.irclogger-logger = {
      description = "Whitequark's IRC Logger (logger)";
      wantedBy = [ "multi-user.target" ];
      after = [ "networking.target" "irclogger-database.service" ];
      serviceConfig = {
        User = cfg.user;
        Group = cfg.group;
        Restart = "always";
      };
      unitConfig.RequiresMountsFor = "${homeDir}";
      preStart = ''
        mkdir -m 0771 -p "${homeDir}"/log
        mkdir -m 0771 -p "${homeDir}"/tmp
        mkdir -m 0771 -p "${homeDir}"/tmp/.sass-cache
        chmod a+x "${homeDir}"
        chmod -R a+x "${homeDir}"/tmp
      '';

      script = ''
        export HOME=/tmp
        export IRCLOGGER_CONFIG="${cfg.config}"
        exec ${pkgs.irclogger}/bin/irclogger-logger
      '';
    };

    systemd.services.irclogger-viewer = {
      description = "Whitequark's IRC Logger (viewer)";
      wantedBy = [ "multi-user.target" ];
      # After the logger, which ensures database exists.
      after = [ "networking.target" "irclogger-database.service" ];
      serviceConfig = {
        User = cfg.user;
        Group = cfg.group;
        Restart = "always";
      };
      preStart = ''
        mkdir -m 0771 -p "${homeDir}"/log
        mkdir -m 0771 -p "${homeDir}"/tmp
        # Socket needs +x to the path.
        chmod a+x "${homeDir}"
        chmod -R a+x "${homeDir}"/tmp
      '';

      script = ''
        export HOME=/tmp
        export IRCLOGGER_CONFIG="${cfg.config}"
        exec ${pkgs.irclogger}/bin/irclogger-viewer \
          -l /${homeDir}/tmp/irclogger-viewer.log \
          -P /${homeDir}/tmp/irclogger-viewer.pid \
          -S /${homeDir}/tmp/irclogger-viewer.sock
      '';
    };

    users.extraUsers = optional (cfg.user == defaultUser)
      { name = defaultUser;
        description = "irclogger user";
        group = defaultUser;
        uid = 521; #config.ids.uids.irclogger;
        home = homeDir;
        createHome = true;
      };

    users.extraGroups = optional (cfg.user == defaultUser)
      { name = defaultUser;
        gid = 521; #config.ids.gids.irclogger;
        members = [ defaultUser ];
      };
  };
}
