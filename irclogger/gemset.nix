{
  backports = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1pinn0m4fmq124adc6xjl2hk9799xq5jw4bva82cdzd4h2hwrgq5";
      type = "gem";
    };
    version = "3.6.0";
  };
  cinch = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "0dmc5rgkdqxd7j3yan7cxfm15vasbphl9mmblpiydwpmjlvjimp1";
      type = "gem";
    };
    version = "2.1.0";
  };
  daemons = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1j1m64pirsldhic6x6sg4lcrmp1bs1ihpd49xm8m1b2rc1c3irzy";
      type = "gem";
    };
    version = "1.1.9";
  };
  em-hiredis = {
    dependencies = ["eventmachine" "hiredis"];
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1zr4wxygzaawm1bscklp0vvbk55y02dzgfhlvgh8pd5hg9y8p3qm";
      type = "gem";
    };
    version = "0.3.0";
  };
  eventmachine = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1wgvhzi27zcszp0gbybvmkxby3wxkrwlkicrjrlyidcj6jz6agd2";
      type = "gem";
    };
    version = "1.2.0.1";
  };
  haml = {
    dependencies = ["tilt"];
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1xmzb0k5q271090crzmv7dbw8ss4289bzxklrc0fhw6pw3kcvc85";
      type = "gem";
    };
    version = "4.0.5";
  };
  hiredis = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "156ahikwrx5sz5mjqqjk68kl1ajd6wxpa44d28n419k89zdzxra1";
      type = "gem";
    };
    version = "0.5.2";
  };
  multi_json = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1ll21dz01jjiplr846n1c8yzb45kj5hcixgb72rz0zg8fyc9g61c";
      type = "gem";
    };
    version = "1.10.1";
  };
  mysql2 = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1v537b7865f4z610rljy8prwmq1yhk3zalp9mcbxn7aqb3g75pra";
      type = "gem";
    };
    version = "0.4.4";
  };
  pg = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "07dv4ma9xd75xpsnnwwg1yrpwpji7ydy0q1d9dl0yfqbzpidrw32";
      type = "gem";
    };
    version = "0.18.4";
  };
  rack = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "19szfw76cscrzjldvw30jp3461zl00w4xvw1x9lsmyp86h1g0jp6";
      type = "gem";
    };
    version = "1.5.2";
  };
  rack-protection = {
    dependencies = ["rack"];
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "0cvb21zz7p9wy23wdav63z5qzfn4nialik22yqp6gihkgfqqrh5r";
      type = "gem";
    };
    version = "1.5.3";
  };
  rack-test = {
    dependencies = ["rack"];
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "01mk715ab5qnqf6va8k3hjsvsmplrfqpz6g58qw4m3l8mim0p4ky";
      type = "gem";
    };
    version = "0.6.2";
  };
  redis = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "06fydrfx65jkha05w14s2bbfxz8f3l9iidzx0j57wwgmwy4vz8rz";
      type = "gem";
    };
    version = "3.1.0";
  };
  sass = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "0k19vj73283i907z4wfkc9qdska2b19z7ps6lcr5s4qzwis1zkmz";
      type = "gem";
    };
    version = "3.3.9";
  };
  sequel = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1rix10khvcbzki3qfp7ih2vxdlcgwjcszq46sdka85jj3jxyq6c4";
      type = "gem";
    };
    version = "4.12.0";
  };
  sinatra = {
    dependencies = ["rack" "rack-protection" "tilt"];
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "0qyna3wzlnvsz69d21lxcm3ixq7db08mi08l0a88011qi4qq701s";
      type = "gem";
    };
    version = "1.4.5";
  };
  sinatra-contrib = {
    dependencies = ["backports" "multi_json" "rack-protection" "rack-test" "sinatra" "tilt"];
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "1dmbxykjqpqfpmacj1nyp3yf5pcr78g33n5q0sxdcdknqq9y4kkg";
      type = "gem";
    };
    version = "1.4.2";
  };
  thin = {
    dependencies = ["daemons" "eventmachine" "rack"];
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "0v90rnnai8sc40c02dxj9aawj2mi1mhjnmi4ws0rg4yrkc6fxvmq";
      type = "gem";
    };
    version = "1.6.2";
  };
  tilt = {
    source = {
      remotes = ["http://rubygems.org"];
      sha256 = "00sr3yy7sbqaq7cb2d2kpycajxqf1b1wr1yy33z4bnzmqii0b0ir";
      type = "gem";
    };
    version = "1.4.1";
  };
}