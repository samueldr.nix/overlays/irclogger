{ lib
, stdenv
, fetchFromGitHub
, bundlerEnv
, makeWrapper
, eject # util-linux, for `cal`.
, ruby
, bundler
, dataDir ? "/var/lib/irclogger"
}:

#
# Notes:
#
#  * Shipping a `bundle` script since none is coming with whitequark's irclogger.
#  * Configuration handling is... lacking upstream. Pass yml file path in env var IRCLOGGER_CONFIG.
#  * The default "thin" configuration has been de-configured a bit.
#
# Most of this "build" has been inspired by the panamax packages.
# Unless there's a cleaner way to do all of this, I think that's it.
#
stdenv.mkDerivation rec {
  name = "irclogger";

  env = bundlerEnv {
    name = "${name}-bundler-env";
    inherit ruby;
    gemdir = ./.;
  };

  buildInputs = [env env.ruby bundler makeWrapper eject];

  src = fetchFromGitHub {
    owner = "whitequark";
    repo = name;
    rev = "f2fbe6fb67555226e0cc48de38b243e23b45b23a";
    sha256 = "1apznnr9k1gxp782l1mc53iflji3yjn4f5pyfsg7vkwbrvs5b2al";
  };

  patches = [
    ./config.fix.patch
    ./config.patch
    ./no-tracking.patch
    ./genericize.patch
    ./dont-daemonize.patch
    ./highlight.patch
  ];

  installPhase = ''
    rm -rf log tmp
    ln -sf ${dataDir}/{log,tmp} .
    ln -sf ${dataDir}/tmp/.sass-cache .

    mkdir -p $out/bin
    mkdir -p $out/share

    cp -prf . $out/share/irclogger
    mkdir -p $out/share/irclogger/bin
    cp ${./bundle} $out/share/irclogger/bin/bundle
    chmod +wx $out/share/irclogger/bin/bundle

    makeWrapper $out/share/irclogger/bin/bundle "$out/bin/irclogger-logger" \
      --run "cd $out/share/irclogger" \
      --prefix "PATH" : "${env.ruby}/bin:$PATH" \
      --prefix "GEM_HOME" : "${env}/${env.ruby.gemPath}" \
      --prefix "GEM_PATH" : "${bundler}/${env.ruby.gemPath}" \
      --add-flags "exec ruby $out/share/irclogger/logger.rb"

    makeWrapper $out/share/irclogger/bin/bundle "$out/bin/irclogger-viewer" \
      --run "cd $out/share/irclogger" \
      --prefix "PATH" : "${env.ruby}/bin:$PATH" \
      --prefix "GEM_HOME" : "${env}/${env.ruby.gemPath}" \
      --prefix "GEM_PATH" : "${bundler}/${env.ruby.gemPath}" \
      --add-flags "exec thin -C config/thin.yml start"

    cat > $out/bin/irclogger-init-database <<EOF
    #!/usr/bin/env bash

    # FIXME : This assumes the username will be irclogger.
    # FIXME : This assumes table name is same as username.
    (
    cat $out/share/irclogger/config/sql/postgresql-schema.sql | grep -v '^#'
    ) | sudo -u irclogger psql
    EOF
    chmod +x $out/bin/irclogger-init-database
  '';

  meta = with stdenv.lib; {
    homepage    = https://github.com/whitequark/irclogger;
    description = "Simple and good-looking IRC log viewer.";
    maintainers = with maintainers; [ /*samueldr*/ ];
    license     = licenses.mit;
    platforms   = platforms.linux;
  };  

  passthru = {
    inherit env ruby;
  };
}
